const tilesProvider = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'

let myMap = L.map('myMap').setView([13.7154952, -89.1469972], 22)
L.tileLayer(tilesProvider,{
	maxZoom: 18,
}).addTo(myMap)

myMap.locate({setView: true, maxZoom: 16});

let iconMarker = L.icon({
	iconUrl: 'icon.png',
	iconSize: [60, 80],
	iconAnchor: [30, 30]
})

let marker = L.marker([13.7154952, -89.1469972]).addTo(myMap)

let marker2 = L.marker([13.707640539857508, -89.13386765683839], { icon: iconMarker}).addTo(myMap)
let marker3 = L.marker([13.71578110302417, -89.14590499269578], { icon: iconMarker}).addTo(myMap)
let marker4 = L.marker([13.717667696576555, -89.15602333644738], { icon: iconMarker}).addTo(myMap)
let marker5 = L.marker([13.717789208421891, -89.1398285740943], { icon: iconMarker}).addTo(myMap)
let marker6 = L.marker([13.718376720572621, -89.13084186840774], { icon: iconMarker}).addTo(myMap)

//